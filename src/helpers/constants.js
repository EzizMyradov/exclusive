export const cardButtons = ['Return To Shop', 'Update Cart']

export const selectedCardSizes = ['XS', 'S', 'M', 'L', 'Xl']

export const footerIcons = ['facebook', 'twitter', 'instagram', 'linkedIn']

export const flashSalesSwiperIcons = [{ icon: 'wishList' }, { icon: 'flipEye' }]

export const myAccountPlaceholders = ['Current Password', 'New Password', 'Confirm New Password']

export const myAccountSubTitles = ['My Profile', 'Address Book', 'My Payment Options']

export const myAccountSecondMenuSubTitles = ['My Returns', 'My Cancellations']

export const productsCheckboxIcons = ['checkedRed', 'checkedDefault']

export const autoplay = {
  delay: 2000,
  disableOnInteraction: false
}
export const pagination = {
  clickable: true
}

export const menus = [
  { id: 0, name: 'Home', path: '/' },
  { id: 1, name: 'Contact', path: '/contact' },
  { id: 2, name: 'About', path: '/about' },
  { id: 3, name: 'Sign Up', path: '/sign-up' }
]

export const icons = [
  { id: 0, name: 'wishList' },
  { id: 1, name: 'card' },
  { id: 2, name: 'user', isActive: true }
]

export const sidebarItems = [
  { id: 0, name: 'Woman’s Fashion', arrow: true },
  { id: 1, name: 'Men’s Fashion', arrow: true },
  { id: 2, name: 'Electronics' },
  { id: 3, name: 'Home & Lifestyle' },
  { id: 4, name: 'Medicine' },
  { id: 5, name: 'Sports & Outdoor' },
  { id: 6, name: 'Baby’s & Toys' },
  { id: 7, name: 'Groceries & Pets' },
  { id: 8, name: 'Health & Beauty' }
]

export const usersDropdownItems = [
  { id: 0, icon: 'bigUser', name: 'Manage My Account' },
  { id: 1, icon: 'mallbag', name: 'My Order' },
  { id: 2, icon: 'cancel', name: 'My Cancellations' },
  { id: 3, icon: 'reviews', name: 'My Reviews' },
  { id: 4, icon: 'logout', name: 'Logout' }
]

export const aboutCounts = [
  { id: 0, icon: 'home', count: '10.5k', title: 'Sallers active our site' },
  { id: 1, icon: 'sale', count: '33k', title: 'Monthly Product Sale' },
  { id: 2, icon: 'bag', count: '45.5k', title: 'Customer active in our site' },
  { id: 3, icon: 'moneyBag', count: '25k', title: 'Anual gross sale in our site' }
]

export const categorySwiperItems = [
  { id: 0, icon: 'phone', name: 'Phones' },
  { id: 1, icon: 'computer', name: 'Computers' },
  { id: 2, icon: 'smartwatch', name: 'SmartWatch' },
  { id: 3, icon: 'camera', name: 'Camera' },
  { id: 4, icon: 'headPhone', name: 'HeadPhones' },
  { id: 5, icon: 'gamepad', name: 'Gaming' }
]

export const musicTImes = [
  { count: '23', title: 'Hours' },
  { count: '05', title: 'Days' },
  { count: '59', title: 'Minutes' },
  { count: '35', title: 'Seconds' }
]

export const services = [
  { icon: 'delivery', title: 'FREE AND FAST DELIVERY', desc: 'Free delivery for all orders over $140' },
  { icon: 'customerService', title: 'FREE AND FAST DELIVERY', desc: 'Free delivery for all orders over $140' },
  { icon: 'guarantee', title: 'MONEY BACK GUARANTEE', desc: 'We return money within 30 days' }
]

export const aboutDescriptions = [
  'Launced in 2015, Exclusive is South Asia’s premier online shopping makterplace with an active presense in Bangladesh. Supported by wide range of tailored marketing, data and service solutions, Exclusive has 10,500 sallers and 300 brands and serves 3 millioons customers across the region.',
  'Exclusive has more than 1 Million products to offer, growing at a very fast. Exclusive offers a diverse assotment in categories ranging from consumer.'
]

export const myAccountInputs = [
  { label: 'First Name', placeholder: 'Md' },
  { label: 'Last Name', placeholder: 'Rimel' },
  { label: 'Email', placeholder: 'rimel1111@gmail.com' },
  { label: 'Address', placeholder: 'Kingston, 5236, United State' }
]

export const signUpInputs = [
  { type: 'email', placeholder: 'Email or Phone Number' },
  { type: 'password', placeholder: 'Password' }
]

export const selectedCardDeliveries = [
  { icon: 'freeDelivery', title: 'Free Delivery', desc: 'Enter your postal code for Delivery Availability' },
  { icon: 'iconReturn', title: 'Return Delivery', desc: 'Free 30 Days Delivery Returns. Details' }
]

export const checkOutInputs = [
  'First Name*',
  'Company Name',
  'Street Address*',
  'Town/City*',
  'Phone Number*',
  'Email Address'
]
