import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path: '/',
    name: 'home_page',
    component: () => import('@/views/HomePageView.vue'),
    meta: { layout: 'default-layout' }
  },
  {
    path: '/sign-up',
    name: 'sign-up',
    component: () => import('@/views/SignUpView.vue'),
    meta: { layout: 'default-layout' }
  },
  {
    path: '/wish-list',
    name: 'wish-list',
    component: () => import('@/views/WishlistsView.vue'),
    meta: { layout: 'default-layout' }
  },
  { path: '/card', name: 'card', component: () => import('@/views/CardView.vue'), meta: { layout: 'default-layout' } },
  {
    path: '/check-out',
    name: 'check-out',
    component: () => import('@/views/CheckOutView.vue'),
    meta: { layout: 'default-layout' }
  },
  {
    path: '/my-account',
    name: 'my-account',
    component: () => import('@/views/MyAccountView.vue'),
    meta: { layout: 'default-layout' }
  },
  {
    path: '/about',
    name: 'about',
    component: () => import('@/views/AboutView.vue'),
    meta: { layout: 'default-layout' }
  },
  {
    path: '/contact',
    name: 'contact',
    component: () => import('@/views/ContactView.vue'),
    meta: { layout: 'default-layout' }
  },
  {
    path: '/selected-card',
    name: 'selected-card',
    component: () => import('@/views/SelectedCardView.vue'),
    meta: { layout: 'default-layout' }
  }
]
const router = createRouter({
  history: createWebHistory(),
  routes: routes
})
export default router
